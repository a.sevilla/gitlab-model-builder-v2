# gitlab-model-builder-v1

## Local run

Create a .env.sh file:

```
export DOTSCIENCE_USERNAME=admin
export DOTSCIENCE_API_KEY=YOUR KEY
export DOTSCIENCE_MODEL_URL=https://dotscience-local-stack.app.cloud.dotscience.net/v2/projects/8a912ed0-803a-468a-9683-4f159b513cfc/files/snapshot/f85cf936-1b20-4442-8beb-45d653f8061b/model

export DOTSCIENCE_MODEL_IMAGE_NAME=quay.io/dotscience-playground/models:00000000-0000-0000-0000-000000000000-f85cf936-1b20-4442-8beb-45d653f8061b

export QUAY_USER=dotscience-playground+gitlabci

export QUAY_PASSWORD=quay-robot-password

export DOTSCIENCE_MODEL_NAME=mnist
```